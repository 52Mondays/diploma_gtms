<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200529151450 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE competition_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE competition_load_date_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE industry_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE users_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE competition (id INT NOT NULL, competition_load_date_id INT DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, deadline TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, grant_size VARCHAR(512) DEFAULT NULL, url VARCHAR(255) DEFAULT NULL, update_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B50A2CB1B463CF15 ON competition (competition_load_date_id)');
        $this->addSql('CREATE TABLE competition_user (competition_id INT NOT NULL, user_id INT NOT NULL, PRIMARY KEY(competition_id, user_id))');
        $this->addSql('CREATE INDEX IDX_83D0485B7B39D312 ON competition_user (competition_id)');
        $this->addSql('CREATE INDEX IDX_83D0485BA76ED395 ON competition_user (user_id)');
        $this->addSql('CREATE TABLE competition_load_date (id INT NOT NULL, date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, status INT DEFAULT 0 NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE industry (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE industry_competition (industry_id INT NOT NULL, competition_id INT NOT NULL, PRIMARY KEY(industry_id, competition_id))');
        $this->addSql('CREATE INDEX IDX_8769D6E72B19A734 ON industry_competition (industry_id)');
        $this->addSql('CREATE INDEX IDX_8769D6E77B39D312 ON industry_competition (competition_id)');
        $this->addSql('CREATE TABLE users (id INT NOT NULL, name VARCHAR(180) NOT NULL, email VARCHAR(180) NOT NULL, email_subscribe BOOLEAN NOT NULL, last_notify_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E9E7927C74 ON users (email)');
        $this->addSql('CREATE TABLE user_industry (user_id INT NOT NULL, industry_id INT NOT NULL, PRIMARY KEY(user_id, industry_id))');
        $this->addSql('CREATE INDEX IDX_2D7788A0A76ED395 ON user_industry (user_id)');
        $this->addSql('CREATE INDEX IDX_2D7788A02B19A734 ON user_industry (industry_id)');
        $this->addSql('ALTER TABLE competition ADD CONSTRAINT FK_B50A2CB1B463CF15 FOREIGN KEY (competition_load_date_id) REFERENCES competition_load_date (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE competition_user ADD CONSTRAINT FK_83D0485B7B39D312 FOREIGN KEY (competition_id) REFERENCES competition (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE competition_user ADD CONSTRAINT FK_83D0485BA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE industry_competition ADD CONSTRAINT FK_8769D6E72B19A734 FOREIGN KEY (industry_id) REFERENCES industry (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE industry_competition ADD CONSTRAINT FK_8769D6E77B39D312 FOREIGN KEY (competition_id) REFERENCES competition (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_industry ADD CONSTRAINT FK_2D7788A0A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_industry ADD CONSTRAINT FK_2D7788A02B19A734 FOREIGN KEY (industry_id) REFERENCES industry (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');

        $this->addSql("INSERT INTO industry (id, name) VALUES
                                (1,      'Математика'),
                                (2,      'Информационные технологии и вычислительные системы'),
                                (3,      'Физика и астрономия'),
                                (4,      'Химия и науки о материалах'),
                                (5,      'Биология'),
                                (6,      'Медицина'),
                                (7,      'Науки о Земле'),
                                (8,      'Лингвистика и культурология'),
                                (9,      'История, археология, этнология, антропология'),
                                (10,     'Философия, политология, социология, правоведение, история науки и техники, науковедение'),
                                (11,     'Психология, фундаментальные проблемы образования, социальные проблемы здоровья и экологии человека'),
                                (12,     'Глобальные проблемы и международные отношения'),
                                (13,     'Инженерные науки'),
                                (14,     'Сельскохозяйственные науки'),
                                (15,     'Экономика');"
        );
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE competition_user DROP CONSTRAINT FK_83D0485B7B39D312');
        $this->addSql('ALTER TABLE industry_competition DROP CONSTRAINT FK_8769D6E77B39D312');
        $this->addSql('ALTER TABLE competition DROP CONSTRAINT FK_B50A2CB1B463CF15');
        $this->addSql('ALTER TABLE industry_competition DROP CONSTRAINT FK_8769D6E72B19A734');
        $this->addSql('ALTER TABLE user_industry DROP CONSTRAINT FK_2D7788A02B19A734');
        $this->addSql('ALTER TABLE competition_user DROP CONSTRAINT FK_83D0485BA76ED395');
        $this->addSql('ALTER TABLE user_industry DROP CONSTRAINT FK_2D7788A0A76ED395');
        $this->addSql('DROP SEQUENCE competition_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE competition_load_date_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE industry_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE users_id_seq CASCADE');
        $this->addSql('DROP TABLE competition');
        $this->addSql('DROP TABLE competition_user');
        $this->addSql('DROP TABLE competition_load_date');
        $this->addSql('DROP TABLE industry');
        $this->addSql('DROP TABLE industry_competition');
        $this->addSql('DROP TABLE users');
        $this->addSql('DROP TABLE user_industry');
    }
}
